Source: balsa
Section: gnome
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Alan Baghumian <alan@technotux.org>, @GNOME_TEAM@
Build-Depends: debhelper-compat (= 13),
               dh-sequence-gnome,
               intltool,
               libcanberra-gtk3-dev,
               libcompfaceg1-dev,
               libesmtp-dev,
               libglib2.0-dev (>= 2.40.0),
               libgmime-3.0-dev (>= 3.2.6),
               libgnutls28-dev,
               libgpgme-dev,
               libgspell-1-dev,
               libgtk-3-dev (>= 3.24.0),
               libgtksourceview-4-dev,
               libical-dev,
               libkrb5-dev,
               libldap2-dev,
               libnotify-dev,
               libsecret-1-dev,
               libsqlite3-dev (>= 3.24.0),
               libssl-dev (>= 1.1.0),
               libwebkit2gtk-4.0-dev,
               libxml2-dev,
               pkg-config,
               python3-html2text,
               yelp-tools,
               zlib1g-dev,
Standards-Version: 4.5.1
X-Ubuntu-Use-Langpack: yes
Vcs-Browser: https://salsa.debian.org/gnome-team/balsa
Vcs-Git: https://salsa.debian.org/gnome-team/balsa.git
Homepage: https://pawsa.fedorapeople.org/balsa/
Rules-Requires-Root: no

Package: balsa
Architecture: any
Provides: imap-client, mail-reader
Depends: balsa-data (= ${source:Version}),
         pinentry-gnome3 | pinentry-x11,
         ${misc:Depends},
         ${shlibs:Depends},
Recommends: ca-certificates, gpgsm, python3-html2text, yelp
Description: e-mail client for GNOME
 Balsa is a highly configurable and robust mail client for the GNOME desktop.
 It supports both POP3 and IMAP servers as well as the mbox, maildir and mh
 local mailbox formats. Balsa also supports SMTP and/or the use of a local MTA
 such as Sendmail.
 .
 Some of Balsa's other features include:
   * Allowing nested mailboxes
   * Printing
   * Spell Checking
   * Multi-threaded mail retrieval
   * MIME support (view images inline, save parts)
   * GPE Palmtop, LDAP, LDIF and vCard address book support
   * Multiple character sets for composing and reading messages
   * File attachments on outgoing messages
   * GPG/OpenPGP mail signing and encryption
 .
 Support for Kerberos and SSL has been enabled in this package.

Package: balsa-data
Architecture: all
Depends: ${misc:Depends}
Replaces: balsa (<< 2.6.2-1)
Breaks: balsa (<< 2.6.2-1)
Description: e-mail client for GNOME -- data files
 Balsa is a highly configurable and robust mail client for the GNOME desktop.
 It supports both POP3 and IMAP servers as well as the mbox, maildir and mh
 local mailbox formats. Balsa also supports SMTP and/or the use of a local MTA
 such as Sendmail.
 .
 This package includes documentation, icons, localizations, and other
 architecture-independent data files for Balsa.
