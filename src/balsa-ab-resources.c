#include <gio/gio.h>

#if defined (__ELF__) && ( __GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ >= 6))
# define SECTION __attribute__ ((section (".gresource.balsa_ab"), aligned (8)))
#else
# define SECTION
#endif

#ifdef _MSC_VER
static const SECTION union { const guint8 data[713]; const double alignment; void * const ptr;}  balsa_ab_resource_data = { {
  0107, 0126, 0141, 0162, 0151, 0141, 0156, 0164, 0000, 0000, 0000, 0000, 0000, 0000, 0000, 0000, 
  0030, 0000, 0000, 0000, 0254, 0000, 0000, 0000, 0000, 0000, 0000, 0050, 0005, 0000, 0000, 0000, 
  0000, 0000, 0000, 0000, 0002, 0000, 0000, 0000, 0002, 0000, 0000, 0000, 0005, 0000, 0000, 0000, 
  0005, 0000, 0000, 0000, 0113, 0120, 0220, 0013, 0001, 0000, 0000, 0000, 0254, 0000, 0000, 0000, 
  0004, 0000, 0114, 0000, 0260, 0000, 0000, 0000, 0264, 0000, 0000, 0000, 0324, 0265, 0002, 0000, 
  0377, 0377, 0377, 0377, 0264, 0000, 0000, 0000, 0001, 0000, 0114, 0000, 0270, 0000, 0000, 0000, 
  0274, 0000, 0000, 0000, 0012, 0067, 0312, 0360, 0004, 0000, 0000, 0000, 0274, 0000, 0000, 0000, 
  0012, 0000, 0166, 0000, 0310, 0000, 0000, 0000, 0260, 0002, 0000, 0000, 0124, 0214, 0331, 0133, 
  0000, 0000, 0000, 0000, 0260, 0002, 0000, 0000, 0010, 0000, 0114, 0000, 0270, 0002, 0000, 0000, 
  0274, 0002, 0000, 0000, 0151, 0377, 0210, 0203, 0003, 0000, 0000, 0000, 0274, 0002, 0000, 0000, 
  0010, 0000, 0114, 0000, 0304, 0002, 0000, 0000, 0310, 0002, 0000, 0000, 0157, 0162, 0147, 0057, 
  0003, 0000, 0000, 0000, 0057, 0000, 0000, 0000, 0000, 0000, 0000, 0000, 0141, 0142, 0055, 0155, 
  0141, 0151, 0156, 0056, 0165, 0151, 0000, 0000, 0146, 0016, 0000, 0000, 0001, 0000, 0000, 0000, 
  0170, 0332, 0325, 0227, 0121, 0117, 0302, 0060, 0020, 0307, 0337, 0375, 0024, 0315, 0236, 0364, 
  0001, 0367, 0156, 0306, 0022, 0024, 0104, 0023, 0143, 0320, 0030, 0137, 0311, 0155, 0073, 0264, 
  0261, 0153, 0147, 0333, 0211, 0174, 0173, 0073, 0106, 0120, 0240, 0135, 0101, 0067, 0023, 0366, 
  0064, 0256, 0327, 0273, 0377, 0375, 0166, 0335, 0216, 0210, 0162, 0215, 0162, 0006, 0051, 0306, 
  0047, 0304, 0134, 0121, 0216, 0274, 0044, 0064, 0353, 0007, 0325, 0115, 0002, 0062, 0250, 0355, 
  0313, 0065, 0125, 0046, 0353, 0345, 0031, 0145, 0330, 0253, 0176, 0375, 0160, 0130, 0072, 0201, 
  0326, 0222, 0046, 0245, 0106, 0302, 0041, 0307, 0176, 0300, 0040, 0101, 0026, 0020, 0055, 0201, 
  0053, 0006, 0032, 0022, 0146, 0214, 0013, 0124, 0101, 0074, 0275, 0066, 0061, 0242, 0160, 0275, 
  0141, 0053, 0220, 0302, 0124, 0123, 0301, 0067, 0255, 0077, 0165, 0354, 0256, 0064, 0010, 0260, 
  0372, 0126, 0327, 0256, 0060, 0247, 0153, 0052, 0014, 0254, 0117, 0335, 0017, 0040, 0313, 0044, 
  0052, 0105, 0022, 0041, 0336, 0232, 0334, 0163, 0043, 0123, 0253, 0176, 0360, 0264, 0312, 0041, 
  0244, 0272, 0040, 0071, 0120, 0116, 0152, 0216, 0032, 0163, 0322, 0043, 0367, 0070, 0047, 0033, 
  0021, 0343, 0251, 0061, 0071, 0301, 0254, 0013, 0255, 0266, 0307, 0316, 0354, 0207, 0162, 0260, 
  0263, 0210, 0247, 0317, 0127, 0040, 0063, 0062, 0130, 0351, 0273, 0064, 0372, 0310, 0351, 0230, 
  0213, 0034, 0053, 0373, 0231, 0127, 0245, 0125, 0012, 0054, 0237, 0154, 0020, 0317, 0051, 0077, 
  0137, 0066, 0022, 0307, 0171, 0357, 0043, 0065, 0001, 0375, 0125, 0207, 0356, 0262, 0377, 0211, 
  0310, 0350, 0323, 0034, 0031, 0016, 0214, 0274, 0227, 0050, 0027, 0344, 0024, 0110, 0041, 0305, 
  0213, 0204, 0274, 0115, 0032, 0270, 0112, 0162, 0014, 0100, 0356, 0206, 0267, 0327, 0033, 0035, 
  0322, 0042, 0007, 0226, 0321, 0331, 0161, 0060, 0030, 0114, 0272, 0143, 0000, 0305, 0061, 0060, 
  0030, 0117, 0106, 0135, 0041, 0170, 0051, 0360, 0030, 0010, 0074, 0226, 0211, 0244, 0051, 0164, 
  0105, 0101, 0326, 0341, 0177, 0115, 0042, 0012, 0235, 0237, 0316, 0006, 0100, 0177, 0377, 0242, 
  0306, 0323, 0211, 0024, 0005, 0112, 0115, 0121, 0371, 0305, 0173, 0121, 0024, 0373, 0005, 0163, 
  0122, 0350, 0266, 0324, 0041, 0062, 0324, 0330, 0102, 0231, 0231, 0077, 0220, 0255, 0104, 0363, 
  0220, 0155, 0223, 0123, 0303, 0074, 0325, 0055, 0217, 0207, 0222, 0352, 0026, 0150, 0274, 0373, 
  0302, 0354, 0311, 0302, 0162, 0006, 0354, 0003, 0345, 0041, 0223, 0354, 0210, 0153, 0271, 0160, 
  0217, 0262, 0216, 0076, 0074, 0000, 0256, 0055, 0151, 0065, 0067, 0066, 0047, 0366, 0222, 0305, 
  0152, 0173, 0365, 0152, 0161, 0153, 0267, 0121, 0355, 0246, 0236, 0372, 0340, 0264, 0123, 0222, 
  0347, 0354, 0154, 0127, 0325, 0111, 0123, 0334, 0040, 0053, 0376, 0273, 0047, 0006, 0211, 0050, 
  0365, 0357, 0341, 0275, 0032, 0311, 0075, 0150, 0214, 0341, 0101, 0027, 0205, 0365, 0275, 0161, 
  0373, 0376, 0147, 0371, 0005, 0101, 0064, 0307, 0020, 0000, 0050, 0165, 0165, 0141, 0171, 0051, 
  0144, 0145, 0163, 0153, 0164, 0157, 0160, 0057, 0004, 0000, 0000, 0000, 0102, 0141, 0154, 0163, 
  0141, 0101, 0142, 0057, 0002, 0000, 0000, 0000
} };
#else /* _MSC_VER */
static const SECTION union { const guint8 data[713]; const double alignment; void * const ptr;}  balsa_ab_resource_data = {
  "\107\126\141\162\151\141\156\164\000\000\000\000\000\000\000\000"
  "\030\000\000\000\254\000\000\000\000\000\000\050\005\000\000\000"
  "\000\000\000\000\002\000\000\000\002\000\000\000\005\000\000\000"
  "\005\000\000\000\113\120\220\013\001\000\000\000\254\000\000\000"
  "\004\000\114\000\260\000\000\000\264\000\000\000\324\265\002\000"
  "\377\377\377\377\264\000\000\000\001\000\114\000\270\000\000\000"
  "\274\000\000\000\012\067\312\360\004\000\000\000\274\000\000\000"
  "\012\000\166\000\310\000\000\000\260\002\000\000\124\214\331\133"
  "\000\000\000\000\260\002\000\000\010\000\114\000\270\002\000\000"
  "\274\002\000\000\151\377\210\203\003\000\000\000\274\002\000\000"
  "\010\000\114\000\304\002\000\000\310\002\000\000\157\162\147\057"
  "\003\000\000\000\057\000\000\000\000\000\000\000\141\142\055\155"
  "\141\151\156\056\165\151\000\000\146\016\000\000\001\000\000\000"
  "\170\332\325\227\121\117\302\060\020\307\337\375\024\315\236\364"
  "\001\367\156\306\022\024\104\023\143\320\030\137\311\155\073\264"
  "\261\153\147\333\211\174\173\073\106\120\240\135\101\067\023\366"
  "\064\256\327\273\377\375\166\335\216\210\162\215\162\006\051\306"
  "\047\304\134\121\216\274\044\064\353\007\325\115\002\062\250\355"
  "\313\065\125\046\353\345\031\145\330\253\176\375\160\130\072\201"
  "\326\222\046\245\106\302\041\307\176\300\040\101\026\020\055\201"
  "\053\006\032\022\146\214\013\124\101\074\275\066\061\242\160\275"
  "\141\053\220\302\124\123\301\067\255\077\165\354\256\064\010\260"
  "\372\126\327\256\060\247\153\052\014\254\117\335\017\040\313\044"
  "\052\105\022\041\336\232\334\163\043\123\253\176\360\264\312\041"
  "\244\272\040\071\120\116\152\216\032\163\322\043\367\070\047\033"
  "\021\343\251\061\071\301\254\013\255\266\307\316\354\207\162\260"
  "\263\210\247\317\127\040\063\062\130\351\273\064\372\310\351\230"
  "\213\034\053\373\231\127\245\125\012\054\237\154\020\317\051\077"
  "\137\066\022\307\171\357\043\065\001\375\125\207\356\262\377\211"
  "\310\350\323\034\031\016\214\274\227\050\027\344\024\110\041\305"
  "\213\204\274\115\032\270\112\162\014\100\356\206\267\327\033\035"
  "\322\042\007\226\321\331\161\060\030\114\272\143\000\305\061\060"
  "\030\117\106\135\041\170\051\360\030\010\074\226\211\244\051\164"
  "\105\101\326\341\177\115\042\012\235\237\316\006\100\177\377\242"
  "\306\323\211\024\005\112\115\121\371\305\173\121\024\373\005\163"
  "\122\350\266\324\041\062\324\330\102\231\231\077\220\255\104\363"
  "\220\155\223\123\303\074\325\055\217\207\222\352\026\150\274\373"
  "\302\354\311\302\162\006\354\003\345\041\223\354\210\153\271\160"
  "\217\262\216\076\074\000\256\055\151\065\067\066\047\366\222\305"
  "\152\173\365\152\161\153\267\121\355\246\236\372\340\264\123\222"
  "\347\354\154\127\325\111\123\334\040\053\376\273\047\006\211\050"
  "\365\357\341\275\032\311\075\150\214\341\101\027\205\365\275\161"
  "\373\376\147\371\005\101\064\307\020\000\050\165\165\141\171\051"
  "\144\145\163\153\164\157\160\057\004\000\000\000\102\141\154\163"
  "\141\101\142\057\002\000\000\000" };
#endif /* !_MSC_VER */

static GStaticResource static_resource = { balsa_ab_resource_data.data, sizeof (balsa_ab_resource_data.data) - 1 /* nul terminator */, NULL, NULL, NULL };
extern GResource *balsa_ab_get_resource (void);
GResource *balsa_ab_get_resource (void)
{
  return g_static_resource_get_resource (&static_resource);
}
/*
  If G_HAS_CONSTRUCTORS is true then the compiler support *both* constructors and
  destructors, in a sane way, including e.g. on library unload. If not you're on
  your own.

  Some compilers need #pragma to handle this, which does not work with macros,
  so the way you need to use this is (for constructors):

  #ifdef G_DEFINE_CONSTRUCTOR_NEEDS_PRAGMA
  #pragma G_DEFINE_CONSTRUCTOR_PRAGMA_ARGS(my_constructor)
  #endif
  G_DEFINE_CONSTRUCTOR(my_constructor)
  static void my_constructor(void) {
   ...
  }

*/

#ifndef __GTK_DOC_IGNORE__

#if  __GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ >= 7)

#define G_HAS_CONSTRUCTORS 1

#define G_DEFINE_CONSTRUCTOR(_func) static void __attribute__((constructor)) _func (void);
#define G_DEFINE_DESTRUCTOR(_func) static void __attribute__((destructor)) _func (void);

#elif defined (_MSC_VER) && (_MSC_VER >= 1500)
/* Visual studio 2008 and later has _Pragma */

#include <stdlib.h>

#define G_HAS_CONSTRUCTORS 1

/* We do some weird things to avoid the constructors being optimized
 * away on VS2015 if WholeProgramOptimization is enabled. First we
 * make a reference to the array from the wrapper to make sure its
 * references. Then we use a pragma to make sure the wrapper function
 * symbol is always included at the link stage. Also, the symbols
 * need to be extern (but not dllexport), even though they are not
 * really used from another object file.
 */

/* We need to account for differences between the mangling of symbols
 * for Win32 (x86) and x64 programs, as symbols on Win32 are prefixed
 * with an underscore but symbols on x64 are not.
 */
#ifdef _WIN64
#define G_MSVC_SYMBOL_PREFIX ""
#else
#define G_MSVC_SYMBOL_PREFIX "_"
#endif

#define G_DEFINE_CONSTRUCTOR(_func) G_MSVC_CTOR (_func, G_MSVC_SYMBOL_PREFIX)
#define G_DEFINE_DESTRUCTOR(_func) G_MSVC_DTOR (_func, G_MSVC_SYMBOL_PREFIX)

#define G_MSVC_CTOR(_func,_sym_prefix) \
  static void _func(void); \
  extern int (* _array ## _func)(void);              \
  int _func ## _wrapper(void) { _func(); g_slist_find (NULL,  _array ## _func); return 0; } \
  __pragma(comment(linker,"/include:" _sym_prefix # _func "_wrapper")) \
  __pragma(section(".CRT$XCU",read)) \
  __declspec(allocate(".CRT$XCU")) int (* _array ## _func)(void) = _func ## _wrapper;

#define G_MSVC_DTOR(_func,_sym_prefix) \
  static void _func(void); \
  extern int (* _array ## _func)(void);              \
  int _func ## _constructor(void) { atexit (_func); g_slist_find (NULL,  _array ## _func); return 0; } \
   __pragma(comment(linker,"/include:" _sym_prefix # _func "_constructor")) \
  __pragma(section(".CRT$XCU",read)) \
  __declspec(allocate(".CRT$XCU")) int (* _array ## _func)(void) = _func ## _constructor;

#elif defined (_MSC_VER)

#define G_HAS_CONSTRUCTORS 1

/* Pre Visual studio 2008 must use #pragma section */
#define G_DEFINE_CONSTRUCTOR_NEEDS_PRAGMA 1
#define G_DEFINE_DESTRUCTOR_NEEDS_PRAGMA 1

#define G_DEFINE_CONSTRUCTOR_PRAGMA_ARGS(_func) \
  section(".CRT$XCU",read)
#define G_DEFINE_CONSTRUCTOR(_func) \
  static void _func(void); \
  static int _func ## _wrapper(void) { _func(); return 0; } \
  __declspec(allocate(".CRT$XCU")) static int (*p)(void) = _func ## _wrapper;

#define G_DEFINE_DESTRUCTOR_PRAGMA_ARGS(_func) \
  section(".CRT$XCU",read)
#define G_DEFINE_DESTRUCTOR(_func) \
  static void _func(void); \
  static int _func ## _constructor(void) { atexit (_func); return 0; } \
  __declspec(allocate(".CRT$XCU")) static int (* _array ## _func)(void) = _func ## _constructor;

#elif defined(__SUNPRO_C)

/* This is not tested, but i believe it should work, based on:
 * http://opensource.apple.com/source/OpenSSL098/OpenSSL098-35/src/fips/fips_premain.c
 */

#define G_HAS_CONSTRUCTORS 1

#define G_DEFINE_CONSTRUCTOR_NEEDS_PRAGMA 1
#define G_DEFINE_DESTRUCTOR_NEEDS_PRAGMA 1

#define G_DEFINE_CONSTRUCTOR_PRAGMA_ARGS(_func) \
  init(_func)
#define G_DEFINE_CONSTRUCTOR(_func) \
  static void _func(void);

#define G_DEFINE_DESTRUCTOR_PRAGMA_ARGS(_func) \
  fini(_func)
#define G_DEFINE_DESTRUCTOR(_func) \
  static void _func(void);

#else

/* constructors not supported for this compiler */

#endif

#endif /* __GTK_DOC_IGNORE__ */

#ifdef G_HAS_CONSTRUCTORS

#ifdef G_DEFINE_CONSTRUCTOR_NEEDS_PRAGMA
#pragma G_DEFINE_CONSTRUCTOR_PRAGMA_ARGS(resource_constructor)
#endif
G_DEFINE_CONSTRUCTOR(resource_constructor)
#ifdef G_DEFINE_DESTRUCTOR_NEEDS_PRAGMA
#pragma G_DEFINE_DESTRUCTOR_PRAGMA_ARGS(resource_destructor)
#endif
G_DEFINE_DESTRUCTOR(resource_destructor)

#else
#warning "Constructor not supported on this compiler, linking in resources will not work"
#endif

static void resource_constructor (void)
{
  g_static_resource_init (&static_resource);
}

static void resource_destructor (void)
{
  g_static_resource_fini (&static_resource);
}
